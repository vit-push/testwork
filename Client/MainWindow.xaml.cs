﻿using System;

using System.Collections.Generic;
using System.Windows;
using System.Configuration;
using System.Windows.Input;
using ClientApp.Model;
using ClientApp.Connection;





namespace ClientApp
{

    // Логика взаимодействия для MainWindow.xaml

    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();

            // читаем адрес сервера к которому обращаться
            try
            {
                var TestServer = ConfigurationManager.AppSettings["server"];
                Comm.ServerComm = TestServer.ToString();
            }
            catch (Exception)
            {
                // строка по умолчанию
                Comm.ServerComm = "localhost:8077";
                MessageBox.Show("Сервер по умолчанию localhost:8077", "Ошибка при чтении настроек",
                MessageBoxButton.OK, MessageBoxImage.Error);
            }

            //обновляем список пользователей
            RefreshUsers();
            // обновляем список городов (для создания нового пользователя)
            RefreshCities();

        }




        // событие при отпускании кнопки клавиатуры (над dgUsers)
        private void dgUsers_KeyUp(object sender, KeyEventArgs e)
        {
            if (dgUsers.Items.Count > 0)
            {
                User selected = dgUsers.SelectedItem as User;
                RefreshUserCities(selected);
            }
        }
        // событие при отпускании кнопки мышки (над dgUsers)
        private void dgUsers_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (dgUsers.Items.Count > 0)
            {
                User selected = dgUsers.SelectedItem as User;

                RefreshUserCities(selected);
            }
        }






        // Обновление списка пользователей
        private void RefreshUsers()
        {
            // получаем список пользователей
            List<User> ulist = Comm.GetAllUsers();

            // передаем в DataGrid
            dgUsers.ItemsSource = ulist;
        }





        // обновление списка городов пользователя
        private void RefreshUserCities(User SelectUser)
        {
            // получаем список городов от сервера
            List<City> clist = Comm.GetUserCities(SelectUser.Id);

            // отправляем полученные данные в GRID
            dgCities.ItemsSource = clist;

            // Меняем заголовок у списка городов
            lUser.Content = $"Города пользователя {SelectUser.Name}"; 
        }





        // Обновление списка всех городов
        private void RefreshCities()
        {
            // получаем всех городов (не указывая конкретного пользователя)
            List<City> clist = Comm.GetUserCities(-1);

            // передаем в DataGrid
            dgNewUserCities.ItemsSource = clist;
        }







        // кнопка добавления нового пользователя
        private void AddNewUser_Click(object sender, RoutedEventArgs e)
        {

            int Birth;
            Int32.TryParse(NewUserBirth.Text, out Birth);

            //список городов пользователя
            List<City> UserCities = new List<City>();
            foreach (City city in dgNewUserCities.SelectedItems)
            {
                UserCities.Add(city);
            }
            // создаем объект с пользователем
            UserToAdd NewUser = new UserToAdd(NewUserName.Text, NewUserGender.Text, Birth, UserCities);

            // добавляем нового пльзователя
            string Result = Comm.AddNewUser(NewUser);

            //обновляем список пользователей
            RefreshUsers();


        }
    }
}
