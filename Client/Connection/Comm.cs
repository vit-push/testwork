﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Windows;
using Newtonsoft.Json;
using ClientApp.Model;

namespace ClientApp.Connection
{
    class Comm
    {
        public static string ServerComm { get; set; }

        // получение списка пользователей
        public static List<User> GetAllUsers()
        {
            // создаем список пользователей
            List<User> ulist = new List<User>();

            // переписать получение из настроек 
            string Request = $"http://{ServerComm}/api/users";

            try
            {
                //запрос
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(Request);
                //ответ
                HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                try
                {
                    //забираем в поток отввет
                    StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream(), Encoding.GetEncoding("utf-8"));

                    //Получаем контент
                    string SerializeUsers = myStreamReader.ReadToEnd();

                    // восстанавливаем объекты из JSON
                    ulist = JsonConvert.DeserializeObject<List<User>>(SerializeUsers);
                }
                catch (Exception e)
                {
                    MessageBox.Show($"Ошибка {Request} \n {e.Source} \n {e.Message} ", "Ошибка при разборе ответа",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                }


            }
            catch (Exception e)
            {
                MessageBox.Show($"Ошибка при получении ответа от {Request} \n {e.Source} \n {e.Message} ", "Ошибка подключения к серверу",
                MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return ulist;
        }


        // добавление нового пльзовател со списком городов
        public static string AddNewUser(UserToAdd NewUser)
        {
            // переписать получение из настроек 
            string RequestURL = $"http://{ServerComm}/AddNewUser";

            //сериализация объектов
            string JsonSerializeData = JsonConvert.SerializeObject(NewUser);

            try
            {
                System.Net.WebRequest reqPOST = System.Net.WebRequest.Create(RequestURL);
                reqPOST.Method = "POST"; // Устанавливаем метод передачи данных в POST
                reqPOST.ContentType = "application / json";
                reqPOST.Timeout = 120000; // Устанавливаем таймаут соединения
                byte[] sentData = System.Text.Encoding.UTF8.GetBytes(JsonSerializeData);
                //byte[] sentData = Encoding.GetEncoding(1251).GetBytes("aid=administrator&pwd=passG&op=login", Encoding.GetEncoding(1251)));

                reqPOST.ContentLength = sentData.Length;
                System.IO.Stream sendStream = reqPOST.GetRequestStream();
                sendStream.Write(sentData, 0, sentData.Length);
                sendStream.Close();

                //ответ
                //HttpWebResponse Response = (HttpWebResponse)Request.GetResponse();
                HttpWebResponse Response = (HttpWebResponse)reqPOST.GetResponse();
                StreamReader Reader = new StreamReader(Response.GetResponseStream(), Encoding.GetEncoding("utf-8"));
                //Получаем ответ с сервера
                string Result = Reader.ReadToEnd();
                return Result;
            }
            catch (Exception e)
            {
                MessageBox.Show($"Ошибка при получении ответа от {RequestURL} \n {e.Source} \n {e.Message} ", "Ошибка подключения к серверу",
                MessageBoxButton.OK, MessageBoxImage.Error);
                return "Ошибка при добавлении нового пользователя";
            }
        }



        // получение списка городов для выбранного пользователя
        public static List<City> GetUserCities(int Id)
        {
            // создаем список пользователей
            List<City> clist = new List<City>();


            // переписать так чтобы адрес сервера был задан в настройках
            string Request = $"http://localhost:8077/api/users/{Id}/cities";

            //if (Id >= 0)
                try
                {

                    //запрос
                    HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(Request);
                    //ответ
                    HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

                    try
                    {
                        //забираем в поток ответ
                        StreamReader myStreamReader = new StreamReader(myHttpWebResponse.GetResponseStream(), Encoding.GetEncoding("utf-8"));

                        //Получаем контент
                        string SerializeUsers = myStreamReader.ReadToEnd();

                        // восстанавливаем объекты из JSON
                        clist = JsonConvert.DeserializeObject<List<City>>(SerializeUsers);
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show($"Ошибка {Request} \n {e.Source} \n {e.Message} ", "Ошибка при разборе ответа",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show($"Ошибка при получении ответа от {Request} \n {e.Source} \n {e.Message} ", "Ошибка подключения к серверу",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                }
            return clist;
        }
    }
}
