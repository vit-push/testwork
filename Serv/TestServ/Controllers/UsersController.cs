﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Data.SQLite;
using System.Data.Common;
using Newtonsoft.Json;
using TestServ.Model;
using Microsoft.Extensions.Options;
using TestServ.Communication;



namespace TestServ.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        //  получение настроек из appsettings.json
        private readonly AppOptions _options; // настройки

        public UsersController(IOptionsMonitor<AppOptions> optionsAccessor)
        {
            _options = optionsAccessor.CurrentValue;

            // отправляем расположение базы в класс CommDB
            //CommDB.stringDBfile = _options.DBfile;
            if (_options.DBfile == "" || _options.DBfile is null)
            {
                // если параметр не задан в appsettings.json то ищем в текущей директории файл testing.db
                CommDB.stringDBfile = AppContext.BaseDirectory + "testing.db";
            }
            else
            {
                // если параметр задан в appsettings.json то ищем по указанному параметру
                CommDB.stringDBfile = _options.DBfile;
            }

        }




        // GET api/users
        // получение списка всех пользователей базы данных
        //[HttpGet]
        [Route("api/users")]
        public ActionResult<string> Get()
        {
            // получение списка пользователей из базы данных 
            List<User> uList = CommDB.GetUserList();

            // сохраняем сериализованный список пользователей 
            // выводим получившийся JSON как результат
            return JsonConvert.SerializeObject(uList); 
        }









        // GET api/users/{id}/cities
        // получение списка городов 
        // если IdUser положительное то выводит города для выбранного пользователя по IdUser
        // если IdUser отрицательное число, тогда выводится список всех городов
        [Route("api/users/{idUser}/cities")]
        public ActionResult<string> Get(int idUser)
        {
            // получение списка городов пользователя из базы данных 
            List<City> cList = CommDB.GetUserCities(idUser);

            // сохраняем сериализованный список пользователей 
            // выводим получившийся JSON как результат
            return JsonConvert.SerializeObject(cList);
        }

 





        [HttpPost, Route("AddNewUser")]
        public string CreateUserName([FromBody]UserToAdd body)
        {
            string st = CommDB.AddUser(body);

            return st;// "Пользователь успешно добавлен";
        }




    }



}
