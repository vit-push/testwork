﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data.Common;
using TestServ.Model;

namespace TestServ.Communication
{
    public class CommDB
    {
        // строка подключения к базе данных 
        public static string stringDBfile { get; set; }

            


        // получение списка пользователей List<User>
        public static List<User> GetUserList()
        {
            // создаем список пользователей
            List<User> UserList = new List<User>();

            // проверяем есть ли файл с базой данных
            if (!System.IO.File.Exists(CommDB.stringDBfile))
            {
                Console.WriteLine("Файла базы данных не существует.");
                //return "Файла базы данных не существует.";
            }
            else
            {
                // если файл есть, открываем базу



                SQLiteConnection SQdb = new SQLiteConnection($"Data Source={CommDB.stringDBfile}; Version=3;");
                try
                {
                    SQdb.Open();
                    Console.WriteLine("База подключена");
                }
                catch (SQLiteException ex)
                {
                    Console.WriteLine("Ошибка1: " + ex.Message);
                }

                // база открылать делаем запрос
                SQLiteCommand command = new SQLiteCommand("SELECT id, name, gender, birth FROM users;", SQdb);
                SQLiteDataReader reader = command.ExecuteReader();

                // обрабатываем полученные строки
                foreach (DbDataRecord record in reader)
                {
                    //раскладываем данные по полям

                    // читаем int'ы
                    int Id = 0;
                    Int32.TryParse(record["id"].ToString(), out Id);
                    int Birth = 0;
                    Int32.TryParse(record["birth"].ToString(), out Birth);

                    // добавление каждого пользователя в список с выделением памяти
                    UserList.Add(new User( Id, 
                                        record["name"].ToString(), 
                                        record["gender"].ToString(), 
                                        Birth) 
                                      );

                }
                // закрываем подключение к БД
                SQdb.Dispose();
                
            }
            // возвращаем список
            return UserList;
        }






        // получение списка городов для пользователя  <IdUser>
        public static List<City> GetUserCities(int IdUser)
        {
            // создаем список пользователей
            List<City> CityList = new List<City>();

            SQLiteConnection SQdb = new SQLiteConnection($"Data Source={CommDB.stringDBfile}; Version=3;");

            if (!System.IO.File.Exists(CommDB.stringDBfile))
            {
                Console.WriteLine("Файла базы данных не существует.");
            }
            else
            {
                // если файл есть, открываем базу
                try
                {
                    SQdb.Open();
                    Console.WriteLine("База подключена");
                }
                catch (SQLiteException ex)
                {
                    Console.WriteLine("Ошибка1: " + ex.Message);
                }

                // база открылать делаем запрос
                SQLiteCommand command;
                if (IdUser >= 0)
                {   // запрашиваем города привязанные к конкретному пользователю
                    command = new SQLiteCommand($"SELECT c.id, c.name FROM Users u INNER JOIN UserCities cu ON cu.idUser = u.id " +
                                         $"INNER JOIN Cities c ON c.id = cu.idCity " +
                                         $"where u.id = {IdUser.ToString()};", SQdb);
                }
                else
                {   // запрашиваем все города в базе данных
                    command = new SQLiteCommand($"SELECT id,name FROM Cities", SQdb);
                }
                SQLiteDataReader reader = command.ExecuteReader();

                // обрабатываем полученные строки
                foreach (DbDataRecord record in reader)
                {
                    int Id = 0;
                    Int32.TryParse(record["id"].ToString(), out Id);

                    // добавление каждого пользователя в список
                    CityList.Add(new City(Id, record["name"].ToString() ));
                }

            }
            
            // возвращаем получившийся результат
            return CityList;
        }





        public static string AddUser(UserToAdd AddUser)
        {

            if (!System.IO.File.Exists(CommDB.stringDBfile))
            {
                return "Ошибка: Файла базы данных не существует.";
            }
            else
            {
                // если файл есть, открываем базу
                try
                {
                    SQLiteConnection Connection = new SQLiteConnection($"Data Source={CommDB.stringDBfile}; Version=3;");
                    Connection.Open();

                    // база открылась делаем запрос на добавление нового пользователя
                    string SQL_INSERT = $"INSERT INTO Users(name, gender, birth)" +
                                        $"VALUES('{AddUser.Name}', '{AddUser.Gender}', {AddUser.Birth.ToString()}) ";
                    SQLiteCommand command = new SQLiteCommand(SQL_INSERT, Connection);

                    // добавляем нового пользователя в базу
                    int CountAdd = command.ExecuteNonQuery();

                    if (CountAdd == 1) // если добавление успешно узнаем номе Id присвоенный пользователю
                    {
                        command.CommandText = "SELECT last_insert_rowid()";
                        int NewUserId = Convert.ToInt32(command.ExecuteScalar());

                        int CountAddCities = 0;
                        foreach (City c in AddUser.UserCities)
                        {
                            // добавляем города
                            command.CommandText = $"INSERT INTO UserCities(IdUser,IdCity) " +
                                                  $"VALUES({NewUserId},{c.Id}) ";
                            CountAdd = command.ExecuteNonQuery();
                            CountAddCities++;
                        }
                        return $"Добавление пользователя прошло успешно. Id нового пользователя {NewUserId}. \n " +
                               $"Количество назначенных городов:{CountAddCities}";
                    }

                }
                catch (SQLiteException ex)
                {
                    return "Ошибка при добавлении нового пользователя: " + ex.Message;
                }


            }

            return "";
        }






        





    }
}
