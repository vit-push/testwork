﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestServ.Model
{
    public class AppOptions
    {
        public AppOptions()
        {
            // Set default value.
            //DBfile = AppContext.BaseDirectory + "testing.db"
        }
        
        // расположение файла с базой данных (по умолчанию в папке с приложением)
        public string DBfile { get; set; }

        
    }
}
