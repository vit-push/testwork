﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestServ.Model
{
    public class User
    {
            public int Id { get; set; }         // ключ в базе
            public string Name { get; set; }    //ФИО
            public string Gender { get; set; }  // пол
            public int Birth { get; set; }      // год рождения

            public User()
            {

            }

            // конструктор без id
            public User(string name, string gender, int birth)
            {
                Name = name;
                Gender = gender;
                Birth = birth;
            }

            //конструктор с id
            public User(int id, string name, string gender, int birth)
            {
                Id = id;
                Name = name;
                Gender = gender;
                Birth = birth;
            }

    }

    public class UsersList
    {
            public List<User> Users { get; set; }
    }



    
}
