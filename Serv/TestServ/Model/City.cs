﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestServ.Model
{
    public class City
    {
        public int      Id      { get; set; }   // Id записи в базе данных
        public string   Name    { get; set; }   // Название города


        public City()
        {

        }

        public City(string name)
        {
            Name = name;
        }

        public City(int id, string name) 
        {
            Id = id;
            Name = name;
        }
    }

    
}
