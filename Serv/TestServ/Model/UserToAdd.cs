﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestServ.Model
{
    public class UserToAdd
    {
        public int Id { get; set; }         // ключ в базе
        public string Name { get; set; }    //ФИО
        public string Gender { get; set; }  // пол
        public int Birth { get; set; }      // год рождения
        public List<City> UserCities { get; set; }



        public UserToAdd()
        {

        }

        // конструктор без id
        public UserToAdd(string name, string gender, int birth, List<City> usercities)
        {
            Name = name;
            Gender = gender;
            Birth = birth;
            UserCities = usercities;
        }

        //конструктор с id
        public UserToAdd(int id, string name, string gender, int birth, List<City> usercities)
        {
            Id = id;
            Name = name;
            Gender = gender;
            Birth = birth;
            UserCities = usercities;
        }

    }
}
